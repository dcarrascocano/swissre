# CLI for Swiss Re


## Table of Contents

- [Requirements](#Requirements)
- [Makefile](#Makefile)
- [Docker](#Docker)
- [Polars](#Polars)
- [Notes](#Notes)

## Requirements

To make easy the use of dockers and CLI, it is recommended install make

Debian/Ubuntu
```Debian/Ubuntu
sudo apt update
sudo apt install make
```
Fedora
```Fedora
sudo dnf install make
```

CentOS/RHEL
```CentOS/RHEL
sudo yum install make
```
## Makefile
*To extract information of log file:*
```make
make analyze` INPUT=<INPUT> OUTPUT=<OUTPUT> ARGS="[--mfip] [--lfip] [--eps] [--bytes]
```

- **INPUT=\<INPUT>**: Replace "\<INPUT>" with the folder or file
- **OUTPUT=\<OUTPUT>**: replace "\<OUTPUT>" with the output folder.
- **ARGS**: Options like "--mfip", "--lfip", "--eps" or "--bytes".

*See help for CLI*
```make
make analyze-help
```
![img.png](images/cli_help.png)

*Unitest for pure python CLI*
```make
make test-cli-native
```
![img.png](images/unittest_cli_native.png)

*Unitest for CLI with polars*
```make
make test-cli-native
```
![img.png](images/unittest_cli_polars.png)

## Docker

Instructions on how to build and run the Docker image for the project.

## Polars

Other option used for build the CLi has been Polars which is a tool to work with data replacing the use of pure python

## Notes
- The use of Polars was used due the analysis focus of the CLI over log files even being a small file.
- All 4 options are processed because the computational cost is small compared to the complexity of the solution according to the arguments. Afterward, it becomes easier to filter according to the request.
